#!/bin/bash

# UPSTREAM DIRECTORIES
UPSTREAM_DIR_ECO_GLOBAL=":/home/rmuchefa/deployments"
UPSTREAM_DIR_UAT=":/home/rmuchefa/deployments"
UPSTREAM_DIR_DEV=":/home/wasadmin/deployments"
UPSTREAM_DIR_LOCAL_ISSISTANT="/opt/deployments/issistant"
UPSTREAM_DIR_Stamford_Bridge=":/opt/deployments/issistant"

if [[ "$3" == "-s" || "$4" == "-s" ]]; then
  SKIP_TESTS=" -DskipTests=true"
else
  SKIP_TESTS=""
fi

# Deploy to Localhost (iSsistant)
if [[ "$2" == "Local_iSsistant" ]]; then
  mvn clean package$SKIP_TESTS
  sudo rsync --progress -cvr $1 $UPSTREAM_DIR_LOCAL
  md5sum $1
  #tail -f /opt/logs/ecosure-v2.log

# Deploy to Stamford Bridge
elif [[ "$2" == "$xxStamfordBridge" ]]; then
  mvn -f $3 clean install$SKIP_TESTS
  rsync --timeout=300 --rsh='ssh -p22' --progress -cvr $1 $2$UPSTREAM_DIR_Stamford_Bridge
  md5sum $1

  if [[ "$1" == $issistantConfigAtifactTo ]]; then
    ssh -t $2 'echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>RESTARTING ISSISTANT CONFIG SERVICE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     sudo service issistant-config restart ;
                     echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TAILING ISSISTANT CONFIG SERVICE (journalctl)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     journalctl -f -u issistant-config ;
                     exec bash -l'
  elif [[ "$1" == "$issistantDiscoveryAtifactTo" ]]; then
    ssh -t $2 'echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>RESTARTING ISSISTANT DISCOVERY SERVICE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     sudo service issistant-discovery restart ;
                     echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TAILING ISSISTANT DISCOVERY SERVICE (journalctl)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     journalctl -f -u issistant-discovery ;
                     exec bash -l'

  elif [[ "$1" == "$issistantGatewayAtifactTo" ]]; then
    ssh -t $2 'echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>RESTARTING ISSISTANT GATEWAY SERVICE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     sudo service issistant-gateway restart ;
                     echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TAILING ISSISTANT GATEWAY SERVICE (journalctl)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     journalctl -f -u issistant-gateway ;
                     exec bash -l'

  elif [[ "$1" == "$issistantReactorAtifactTo" ]]; then
    ssh -t $2 'echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>RESTARTING ISSISTANT REACTOR SERVICE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     sudo service issistant-reactor restart ;
                     echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TAILING REACTOR DISCOVERY SERVICE (journalctl)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     journalctl -f -u issistant-reactor ;
                     exec bash -l'

  elif [[ "$1" == "$issistantNotificationsAtifactTo" ]]; then
    ssh -t $2 'echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>RESTARTING ISSISTANT NOTIFICATIONS SERVICE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     sudo service issistant-notifications restart ;
                     echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TAILING ISSISTANT NOTIFICATIONS SERVICE (journalctl)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     journalctl -f -u issistant-notifications ;
                     exec bash -l'

  elif [[ "$1" == "$issistantSecurityAtifactTo" ]]; then
    ssh -t $2 'echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>RESTARTING ISSISTANT SECURITY SERVICE<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     sudo service issistant-security restart ;
                     echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>TAILING ISSISTANT SECURITY SERVICE (journalctl)<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" ;
                     journalctl -f -u issistant-security ;
                     exec bash -l'
  fi

# Deploy to 194
elif [[ "$2" == "$xx194" ]]; then
  mvn -f $3 clean install$SKIP_TESTS
  rsync --progress -cvr $1 $2$UPSTREAM_DIR_DEV
  md5sum $1
  ssh $2

# Deploy to 63
elif [[ "$2" == "$xx63" ]]; then
  mvn clean install$SKIP_TESTS
  rsync --progress -cvr $1 $2$UPSTREAM_DIR_DEV
  md5sum $1
  ssh $2

#Deploy to 189
elif [[ "$2" == "$xx189" ]]; then
  mvn -f $3 clean install$SKIP_TESTS
  rsync --progress -cvr $1 $2$UPSTREAM_DIR_UAT
  md5sum $1
  ssh $2

# Deploy to 191
elif [[ "$2" == "$xx191" ]]; then
  mvn clean package$SKIP_TESTS
  rsync --progress -cvr $1 $2$UPSTREAM_DIR_DEV
  md5sum $1
  ssh $2

# Deploy to 180
elif [[ "$2" == "$xx180" ]]; then
  mvn clean package$SKIP_TESTS
  scp -r $1 $2$UPSTREAM_DIR_UAT
  md5sum $1
  ssh $2

# Deploy to 145, 147
elif [[ "$2" == "$xx145" || "$2" == "$xx147" ]]; then
  mvn -f $3 clean install $SKIP_TESTS
  rsync -Pazhv $1 $2$UPSTREAM_DIR_ECO_GLOBAL
  md5sum $1
  ssh $2

# Deploy Admin Portal to 147
elif [[ "$3" == "$xx147" && "$1" == "--admin_portal" ]]; then
  rsync -Pazhv $2 $3$UPSTREAM_DIR_ECO_GLOBAL'/admin-portal'
  ssh $3

# Deploy Customer Website to 147
elif [[ "$3" == "$xx147" && "$1" == "--customer_website" ]]; then
  rsync -Pazhv $2 $3$UPSTREAM_DIR_ECO_GLOBAL'/customer-website'
  ssh $3

fi
