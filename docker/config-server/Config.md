# Create and run Config server container

### Create network (If it doesn't exist)
    docker network create rynmaker-network

### Create and build dockerfile
    go to dir with Dockerfile
    docker build . -t rynmaker-config-server

### Run docker container
    docker run --name rynmaker-config-server \
    --network rynmaker-network \
    -v /home/developer/apps/rynmaker/config-server/:/logs/rynmaker/config-server \
    -p 7002:7002 \
    -d rynmaker-config-server