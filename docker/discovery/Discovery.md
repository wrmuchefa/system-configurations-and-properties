# Create and run Discovery container

### Create network (If it doesn't exist)
    docker network create rynmaker-network

### Create and build dockerfile
    go to dir with Dockerfile
    docker build . -t rynmaker-discovery

### Run docker container
    docker run --name rynmaker-discovery \
    --network rynmaker-network \
    -v /home/developer/apps/rynmaker/discovery/:/logs/rynmaker/discovery \
    -p 7001:7001 \
    -d rynmaker-discovery