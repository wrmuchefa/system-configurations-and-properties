# Create and run Access manager container

### Create network (If it doesn't exist)
    docker network create rynmaker-network

### Create and build dockerfile
    go to dir with Dockerfile
    docker build . -t rynmaker-access-manager

### Run docker container
    docker run --name rynmaker-access-manager \
    --network rynmaker-network \
    -v /home/developer/apps/rynmaker/access-manager/:/logs/rynmaker/access-manager \
    -p 7003:7003 \
    -d rynmaker-access-manager