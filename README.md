###### **Additional Technical Information for the Ecosure Life Global System**

1. Architecture
- Java 8
- The system leverages Spring Cloud Microservices Architecture (Discovery First Approach)
- Implements Spring Cloud Discovery, Config & Gateway
- Utilises a standalone Auth Server (Keycloak 15.0.1)
- MySQL Servers for RDMS 
- The System is broken down to the following individual microservices, with more to be added as the scope grows
    - Discovery Service
    - Config Service
    - API Gateway 
    - Authentiction Server (Keycloak)
    - Authentiction Service
    - Core Service
    - User Management Service
    - Policies Service
    - Payments Service
    - Documents Service
    - Notifications Service
    
2. Deployment Structure
- To go through 3 Environments of the SDLC (Development, PreProduction & Production)
- 3 Application Servers (AppSvr1, AppSvr2, AppSvr3)
- 2 Mirrored Database Servers (DbSvr1, DbSvr2), we use one (DbSvr1) and it is mirrored to the backup server (DbSvr2) in near-real-time

2.1 Application Server Allocation
- The microservices are going to be deployed on respective Application Servers as follows (Ideally):

2.1.1 Application Servers Pool (AppSvr1, AppSvr2. AppSvr3)

2.1.1a) AppSvr1
- Discovery Service
- Config Service
- Authentication Server
- Authentication Service

2.1.1b) AppSvr2
- Core Service
- User Management Service
- Policies Service
- Payments Service

2.1.1c) AppSvr3
- API Gateway
- Reports Server
- Documents Server
- Notification Service
- Admin Portal

2.1.2) Database Servers Pool (DbSvr1, DbSvr2)
- Keycloak Database (schema name : ecosure_global_keycloak)
- Core Database (schema name : ecosure_global_core)
- User Management Database (schema name : ecosure_global_user_management)
- Keycloak Documents (schema name : ecosure_global_documents)
- Notifications Database (schema name : ecosure_global_notification)
- Payments Database (schema name : ecosure_global_payments)
- Policies Database (schema name : ecosure_global_policies)








