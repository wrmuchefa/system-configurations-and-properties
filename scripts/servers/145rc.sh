# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias x="exit"
alias please='echo "****SUDO$(fc -ln -1)<----***"; sudo $(fc -ln -1)'
alias kkk="please"
alias k="please"
alias c="clear"

#Sasaisure SERVER-SIDE
#145
#Service Deploy & Start

alias dodeploy_discovery='sudo cp deployments/discovery.jar /data/sasaisure/applications/discovery/ ; sudo service discovery restart; sudo journalctl -fu discovery'
alias dodeploy_config='sudo cp deployments/server.jar /data/sasaisure/applications/config/ ; sudo service config-server restart; sudo journalctl -fu config-server'
alias dodeploy_keycloak='sudo cp deployments/keycloak.jar /data/sasaisure/applications/keycloak/ ; sudo service keycloak restart; sudo journalctl -fu keycloak'
alias dodeploy_policyManager='sudo cp deployments/policy-manager.jar /data/sasaisure/applications/policy-manager/ ; sudo service policy-manager restart; sudo journalctl -fu policy-manager'
alias dodeploy_core='sudo cp deployments/core.jar /data/sasaisure/applications/core/ ; sudo service core restart; sudo journalctl -fu core'


#Service Stop
alias dostop_discovery='sudo service discovery stop'
alias dostop_config='sudo service config-server stop'
alias dostop_keycloak='sudo service keycloak stop'
alias dostop_core='sudo service core stop'
alias dostop_policy_manager='sudo service policy-manager stop'

#Service Restart
alias dorestart_config='sudo service config-server restart; sudo journalctl -fu config-server'
alias dorestart_discovery='sudo service discovery restart; sudo journalctl -fu discovery'
alias dorestart_core='sudo service core restart; sudo journalctl -fu core'
alias dorestart_policyManager='sudo service policy-manager restart; sudo journalctl -fu policy-manager'
alias dorestart_keycloak='sudo service keycloak restart; sudo journalctl -fu keycloak'

#Log tail
alias dotail_discovery='tail -fn1000 /data/sasaisure/logs/discovery/application.log'
alias dotail_config='tail -fn1000 /data/sasaisure/logs/config-server/application.log'
alias dotail_keycloak='tail -fn1000 /data/sasaisure/logs/keycloak/application.log'
alias dotail_core='tail -fn1000 /data/sasaisure/logs/core/application.log'
alias dotail_policy_manager='tail -fn1000 /data/sasaisure/logs/policy-manager/application.log'