# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias x="exit"
alias please='echo "****SUDO$(fc -ln -1)<----***"; sudo $(fc -ln -1)'
alias kkk="please"
alias k="please"
alias c="clear"

#Sasaisure SERVER-SIDE
#147

#Service Deploy & Start
alias dodeploy_gateway='sudo cp deployments/gateway.jar /data/sasaisure/applications/gateway/ ; sudo service gateway restart; sudo journalctl -fu gateway'
alias dodeploy_notificationManager='sudo cp deployments/notification-manager.jar /data/sasaisure/applications/notification-manager/ ; sudo service notification-manager restart; sudo journalctl -fu notification-manager'
alias dodeploy_documentManager='sudo cp deployments/document-manager.jar /data/sasaisure/applications/document-manager/ ; sudo service document-manager restart; sudo journalctl -fu document-manager'
alias dodeploy_accessManager='sudo cp deployments/access-manager.jar /data/sasaisure/applications/access-manager/ ; sudo service access-manager restart; sudo journalctl -fu access-manager'
alias dodeploy_paymentManager='sudo cp deployments/payment-manager.jar /data/sasaisure/applications/payment-manager/ ; sudo service payment-manager restart; sudo journalctl -fu payment-manager'
alias dodeploy_reportManager='sudo cp deployments/report-manager.jar /data/sasaisure/applications/report-manager/ ; sudo service report-manager restart; sudo journalctl -fu report-manager'
alias dodeploy_admin_portal='rm -r /data/sasaisure/ui/admin-portal/dev/public_html/* ; rsync -avzh deployments/admin-portal/dev/ /data/sasaisure/ui/admin-portal/dev/public_html/'
alias dodeploy_customer_website='rm -r /data/sasaisure/ui/customer-website/dev/public_html/* ; rsync -avzh deployments/customer-website/dev/ /data/sasaisure/ui/customer-website/dev/public_html/'

#Service Stop
alias dostop_gateway='sudo service gateway stop'
alias dostop_notificationManager='sudo service notification-manager stop'
alias dostop_documentManager='sudo service document-manager stop'
alias dostop_reportManager='sudo service report-manager stop'
alias dostop_accessManager='sudo service access-manager stop'
alias dostop_paymentManager='sudo service payment-manager stop'

#Service Restart
alias dorestart_documentManager='sudo service document-manager restart; sudo journalctl -fu document-manager'
alias dorestart_notificationManager='sudo service notification-manager restart; sudo journalctl -fu notification-manager'
alias dorestart_reportManager='sudo service report-manager restart; sudo journalctl -fu report-manager'
alias dorestart_gateway='sudo service gateway restart; sudo journalctl -fu gateway'
alias dorestart_accessManager='sudo service access-manager restart; sudo journalctl -fu access-manager'
alias dorestart_paymentManager='sudo service payment-manager restart; sudo journalctl -fu payment-manager'

#Log Tail
alias dotail_gateway='tail -fn1000 /data/sasaisure/logs/gateway/application.log'
alias dotail_notificationManager='tail -fn1000 /data/sasaisure/logs/notification-manager/application.log'
alias dotail_documentManager='tail -fn1000 /data/sasaisure/logs/document-manager/application.log'
alias dotail_reportManager='tail -fn1000 /data/sasaisure/logs/report-manager/application.log'
alias dotail_paymentManager='tail -fn1000 /data/sasaisure/logs/payment-manager/application.log'
alias dotail_accessManager='tail -fn1000 /data/sasaisure/logs/access-manager/application.log'