     # include dev bashrc if it exists
     if [ -f $HOME/.bashrc-dev ]; then
         . $HOME/.bashrc-dev
     fi
     # include thwalasonke bashrc if it exists
     if [ -f $HOME/.bashrc-thwalasonke ]; then
         . $HOME/.bashrc-thwalasonke
     fi
     # include sasaisure bashrc if it exists
     if [ -f $HOME/.bashrc-sasaisure ]; then
         . $HOME/.bashrc-sasaisure
     fi
     # include mebanbo bashrc if it exists
     if [ -f $HOME/.bashrc-mebanbo ]; then
         . $HOME/.bashrc-mebanbo
     fi
     # include rynmaker_env if it exists
     if [ -f $HOME/.rynmaker_env ]; then
         . $HOME/.rynmaker_env
     fi

